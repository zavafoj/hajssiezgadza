#ifndef WHIPROUND_H
#define WHIPROUND_H

#include <QObject>
#include <QtAlgorithms>

#include "data.h"
#include "exceptions.h"

//!
//! \brief The WhipRound class
//! \author Wojciech Ossowski
class WhipRound : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Wojciech Ossowski")

    // communication
public:
    explicit WhipRound(QObject *parent = 0);
    ~WhipRound();

public slots:
    void calculateAll();
    void viewBalance();
    void clearList();
    void addToList(QString name,
                   int amount);
    void removeFromList(int &id);

    // algo
private:
    void dataFloat();
    void doWithdraw(Data *element1,
                    Data *element2);
    int calculateSum();
    void divideRest();
    bool isOver();
    void sortList();

    // fields
private:
    //!
    //! \brief list
    //! lish of whole customers
    QList<Data *> list;
    //!
    //! \brief totalValue
    //! amount of total sum value
    int totalValue;
    //!
    //! \brief totalSum
    //! total sum of customers amount
    int totalSum;
    //!
    //! \brief rest
    //! rest, that customer receive
    int rest;
    //!
    //! \brief tempRest
    //! temporary rest used in algorithm
    int tempRest;
    //!
    //! \brief unit
    //! currency unit
    QString unit;

signals:
    
public slots:
    
};

#endif // WHIPROUND_H
