#include "whipround.h"
#include <QDebug>

WhipRound::WhipRound(QObject *parent) :
    QObject(parent)
{
    this->addToList("Krzchu", 5200);
    this->addToList("Zbychu", 3300);
    this->addToList("Jaromir", 4400);
    this->addToList("Zygfryd", 200);

    this->totalValue  = 12000;

    this->calculateAll();
}

WhipRound::~WhipRound()
{

}

/*
 * Communication
 */

//!
//! \brief WhipRound::calculateAll
//!
void WhipRound::calculateAll()
{
    this->totalSum = this->calculateSum();
    int additionalAmount = this->list.size() - (this->totalValue % list.size());
    if(this->totalValue % this->list.size() != 0)
    {
        totalValue += additionalAmount;
    }

    this->rest = this->totalSum - this->totalValue;

    if(this->rest < 0)
    {
        throw NotEnoughMoney;
    }

    this->tempRest = this->rest;

    int value = (int) (this->totalValue/this->list.size());

    foreach(Data *d, this->list)
    {
        d->setBalance(d->getValue() - value);
    }

    this->divideRest();

    while(!this->isOver())
    {
        this->dataFloat();
    }

    this->viewBalance();
}

//!
//! \brief WhipRound::viewBalance
//!
void WhipRound::viewBalance()
{
    foreach (Data* d, this->list) {
        if(d->getWillGetFromCash() > 0)
        {
            qDebug() << d->getName() << "will receive"
                     << d->getWillGetFromCash() << "from cash";
        }
        if(d->getBuddyItemList().size() > 0)
        {
            foreach(BuddyItem *bi, d->getBuddyItemList())
            {
                qDebug() << d->getName() << "has to give" <<
                            bi->getAmount() << "to"
                            << bi->getName();
            }
        }
    }
}

//!
//! \brief WhipRound::clearList
//!
void WhipRound::clearList()
{
    this->list.clear();
}

//!
//! \brief WhipRound::addToList
//! \param name
//! \param amount
//!
void WhipRound::addToList(QString name,
                          int amount)
{
    this->list.push_back(new Data(name, amount));
}

//!
//! \brief WhipRound::removeFromList
//! \param id
//!
void WhipRound::removeFromList(int &id)
{
    this->list.removeOne(this->list.at(id));
}

/*
 * ALGO
 */

//!
//! \brief WhipRound::sortList
//!
void WhipRound::sortList()
{
    qSort(this->list.begin(),
          this->list.end(),
          qGreater<Data*>());
}

//!
//! \brief WhipRound::dataFloat
//!
void WhipRound::dataFloat()
{
    this->sortList();
    int i;
    int j;
    for(i = 0; i <list.size(); i++)
    {
        for(j = 0; j < list.size(); j++)
        {
            if(list.at(i)->getBalance() > 0 &&
               list.at(j)->getBalance() < 0)
            {
                this->doWithdraw(list.at(i), list.at(j));
                this->sortList();
            }
        }
    }
}

//!
//! \brief WhipRound::doWithdraw
//! This methode divides data-float between two custommers
//! \param element1 rich customer
//! \param element2 poor customer
//!
void WhipRound::doWithdraw(Data *element1,
                Data *element2)
{
    int difference = element1->getBalance() + element2->getBalance();
    if(difference >= 0)
    {
        element2->addIntoBuddyList(element1->getName(),
                                   -element2->getBalance());
        element1->setBalance(difference);
        element2->setBalance(0);
    }
    else
    {
        element2->addIntoBuddyList(element1->getName(),
                                   element1->getBalance());
        element2->setBalance(difference);
        element1->setBalance(0);
    }
}

//!
//! \brief WhipRound::calculateSum
//! Calculates total sum of whip-round customers
//! \return total sum
//!
int WhipRound::calculateSum()
{
    int temp = 0;
    foreach (Data *d, this->list)
    {
        temp += d->getValue();
    }
    return temp;
}

//!
//! \brief WhipRound::divideRest
//!
void WhipRound::divideRest()
{
    this->sortList();
    while(this->tempRest > 0 && !this->isOver())
    {
        foreach(Data *d, this->list)
        {
            if(d->getBalance() > 0)
            {
                if(d->getBalance() > this->tempRest)
                {
                    d->setWillGetFromCash(d->getWillGetFromCash()
                                          + tempRest);
                    d->setBalance(d->getBalance() - tempRest);
                    tempRest = 0;
                }
                else if(d->getBalance() <= tempRest)
                {
                    d->setWillGetFromCash(d->getWillGetFromCash()
                                          + d->getBalance());
                    this->tempRest -= d->getBalance();
                    d->setBalance(0);
                }
            }
        }
    }
}

//!
//! \brief WhipRound::isOver
//! Checkout method if dataFloat is over
//! \return is data float over
//!
bool WhipRound::isOver()
{
    foreach (Data *d, this->list)
    {
        if(d->getBalance() != 0)
        {
            return false;
        }
    }
    return true;
}
