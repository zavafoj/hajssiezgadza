#ifndef DATA_H
#define DATA_H

#include <QObject>

#include "buddyitem.h"

//!
//! \brief The Data class
//! \author Wojciech Ossowski
class Data : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Wojciech Ossowski")

public:
    explicit Data(QString &insertName,
                  int &insertValue,
                  QObject *parent = 0);
    /*
     * setters
     */
    void setName(QString name);
    void setValue(int value);
    void setBalance(int balance);
    void setWillGetFromCash(int willGetFromCash);
    void addIntoBuddyList(QString ownerName,
                          int value);

    /*
     * getters
     */
    QString getName();
    int getValue();
    int getBalance();
    int getWillGetFromCash();
    QList<BuddyItem *> getBuddyItemList();

    bool operator> (Data *d);
    bool operator< (Data *d);
    bool operator== (Data *d);

private:
    QString name;
    int value;
    int balance;
    int willGetFromCash;
    QList<BuddyItem *> buddyItemList;


signals:
    
public slots:
    
};

#endif // DATA_H
