#include "buddyitem.h"

//!
//! \brief BuddyItem::BuddyItem Creates a new Buddy Item
//! object with name and amount of what it has parameter.
//! \param name buddy's name
//! \param amount amount of cash, that buddy gives
//! \param parent
//!
BuddyItem::BuddyItem(QString &name, int &amount, QObject *parent) :
    QObject(parent)
{
    this->name = name;
    this->amount = amount;
}

//!
//! \brief BuddyItem::getName
//! \return name of buddy
//!
QString BuddyItem::getName()
{
    return this->name;
}

//!
//! \brief BuddyItem::getAmount
//! \return cash that buddy pays to another
//!
int BuddyItem::getAmount()
{
    return this->amount;
}
