#-------------------------------------------------
#
# Project created by QtCreator 2013-08-17T23:45:29
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = projectCore
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    data.cpp \
    buddyitem.cpp \
    whipround.cpp

HEADERS += \
    data.h \
    buddyitem.h \
    whipround.h \
    exceptions.h
