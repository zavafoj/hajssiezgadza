#ifndef BUDDYITEM_H
#define BUDDYITEM_H

#include <QObject>

//!
//! \brief The BuddyItem class
//! \author Wojciech Ossowski
class BuddyItem : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("Author", "Wojciech Ossowski")

public:
    explicit BuddyItem(QString &name, int &amount, QObject *parent = 0);
    QString getName();
    int getAmount();

private:
    int amount;
    QString name;

signals:

    
public slots:
    
};

#endif // BUDDYITEM_H
