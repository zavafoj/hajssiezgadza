#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

enum WhipRoundException {
    NotEnoughMoney,
    Undividable
};

#endif // EXCEPTIONS_H
