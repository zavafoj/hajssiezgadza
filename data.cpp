#include "data.h"

//!
//! \brief Data::Data
//! \param insertName
//! \param insertValue
//! \param parent
//!
Data::Data(QString &insertName,
           int &insertValue, QObject *parent) :
    QObject(parent)
{
    this->name = insertName;
    this->value = insertValue;
    this->willGetFromCash = 0;
}

/*
 * setters
 */
//!
//! \brief Data::setName
//! \param name
//!
void Data::setName(QString name)
{
    this->name = name;
}

//!
//! \brief Data::setValue
//! \param value
//!
void Data::setValue(int value)
{
    this->value = value;
}

//!
//! \brief Data::setBalance
//! \param balance
//!
void Data::setBalance(int balance)
{
    this->balance = balance;
}

//!
//! \brief setWillGetFromCash
//! \param willGetFromCash
//!
void Data::setWillGetFromCash(int willGetFromCash)
{
    this->willGetFromCash = willGetFromCash;
}

//!
//! \brief Data::addIntoBuddyList
//! \param ownerName
//! \param value
//!
void Data::addIntoBuddyList(QString ownerName,
                      int value)
{

    BuddyItem *bi = new BuddyItem(ownerName,value);
    this->buddyItemList.push_back(bi);
}

/*
 * getters
 */

//!
//! \brief Data::getName
//! \return
//!
QString Data::getName()
{
    return this->name;
}

//!
//! \brief Data::getValue
//! \return
//!
int Data::getValue()
{
    return this->value;
}

//!
//! \brief Data::getBalance
//! \return
//!
int Data::getBalance()
{
    return this->balance;
}

//!
//! \brief getWillGetFromCash
//! \return
//!
int Data::getWillGetFromCash()
{
    return this->willGetFromCash;
}

QList<BuddyItem *> Data::getBuddyItemList()
{
    return this->buddyItemList;
}

/*
 * params
 */


bool Data::operator== (Data *d)
{
    return this->getValue() == d->getValue();
}

bool Data::operator> (Data *d)
{
    return this->getValue() > d->getValue();
}

bool Data::operator< (Data *d)
{
    return this->getValue() < d->getValue();
}

